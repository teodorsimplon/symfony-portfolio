<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Portfolio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RegisterNewProjectType;

class PortfolioController extends AbstractController
{
    #[Route('/', name: 'app_accueil')]
    public function index(): Response
    {
        return $this->render('portfolio/index.html.twig', [
            'controller_name' => 'PortfolioController',
        ]);
    }

    #[Route('/portfolio', name: 'app_portfolio')]
    public function portfolio(ManagerRegistry $doctrine): Response
    {
        $projets = $doctrine->getRepository(Portfolio::class)->findAll();
       
        return $this->render('portfolio/portfolio.html.twig', [
            'controller_name' => 'PortfolioController',
            'data' => $projets,


        ]);
    }
    #[Route('/add-project', name: 'app_add_project')]
    public function addNewProject(Request $request, EntityManagerInterface $entityManager): Response
    {
        $portfolio = new Portfolio();

        $form= $this->createForm(RegisterNewProjectType::class, $portfolio);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($portfolio);
            $entityManager->flush();

        }
        

        return $this->render('portfolio/addProject.html.twig', [
            'controller_name' => 'PortfolioController',
            'form' => $form->createView()
        ]);
    }

}
