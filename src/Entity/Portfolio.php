<?php

namespace App\Entity;

use App\Repository\PortfolioRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PortfolioRepository::class)]
class Portfolio
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'string', length: 2000)]
    private $description;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl2;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $linkRepo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $linkSite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImgUrl1(): ?string
    {
        return $this->imgUrl1;
    }

    public function setImgUrl1(?string $imgUrl1): self
    {
        $this->imgUrl1 = $imgUrl1;

        return $this;
    }

    public function getImgUrl2(): ?string
    {
        return $this->imgUrl2;
    }

    public function setImgUrl2(?string $imgUrl2): self
    {
        $this->imgUrl2 = $imgUrl2;

        return $this;
    }

    public function getLinkRepo(): ?string
    {
        return $this->linkRepo;
    }

    public function setLinkRepo(?string $linkRepo): self
    {
        $this->linkRepo = $linkRepo;

        return $this;
    }

    public function getLinkSite(): ?string
    {
        return $this->linkSite;
    }

    public function setLinkSite(?string $linkSite): self
    {
        $this->linkSite = $linkSite;

        return $this;
    }
}
